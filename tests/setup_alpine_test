#!/usr/bin/env atf-sh

. $(atf_get_srcdir)/test_env.sh
init_tests \
	setup_alpine_usage \
	setup_alpine_quick \
	setup_alpine_create_answerfile \
	setup_alpine_kvm_clock \
	setup_alpine_restart_network

setup_alpine_usage_body() {
	test_usage setup-alpine
}

setup_alpine_quick_body() {
	init_env
	mkdir -p sys/class/net/lo sys/class/net/eth0
	echo 1 >sys/class/net/lo/ifindex
	echo 2 >sys/class/net/eth0/ifindex
	echo down >sys/class/net/lo/operstate
	echo down >sys/class/net/eth0/operstate
	SSH_CONNECTION= atf_check -s exit:0 \
		-o match:"openrc boot" \
		-o match:"openrc default" \
		-e empty \
		setup-alpine -q

	atf_check -s exit:0 \
		rc-service --quiet hostname status
	atf_check -s exit:0 \
		rc-service --quiet networking status
}

setup_alpine_create_answerfile_body() {
	init_env
	atf_check -s exit:0 \
		-o match:"Answer file answers has been created"  \
		setup-alpine -c answers

	mkdir -p usr/share/zoneinfo/
	touch usr/share/zoneinfo/UTC

	echo 'USERSSHKEY="ssh-rsa blahbla user@example.com"' >> answers
	echo 'ROOTSSHKEY="ssh-rsa blahbla user@example.com"' >> answers

	SSH_CONNECTION= atf_check -s exit:0 \
		-o match:"Starting hostname" \
		-o match:"Starting mdev" \
		-o match:"Added mirror" \
		-o match:"adduser" \
		-o match:"apk add.*openssh" \
		setup-alpine -f answers
	grep -x "ssh-rsa blahbla user@example.com" home/juser/.ssh/authorized_keys \
		|| atf_fail "ssh key not set for juser"
	grep -x "ssh-rsa blahbla user@example.com" root/.ssh/authorized_keys \
		|| atf_fail "ssh key not set for root"
	for file in home/juser root/.ssh/authorized_keys; do
		grep -x "+.*$file" etc/apk/protected_paths.d/lbu.list \
			|| atf_fail "$file was not added to lbu.list"
	done
}

setup_alpine_kvm_clock_body() {
	init_env

	mkdir -p sys/class/net/lo sys/class/net/eth0
	echo 1 >sys/class/net/lo/ifindex
	echo 2 >sys/class/net/eth0/ifindex
	echo down >sys/class/net/lo/operstate
	echo down >sys/class/net/eth0/operstate

	mkdir -p sys/devices/system/clocksource/clocksource0
	echo kvm-clock > sys/devices/system/clocksource/clocksource0/current_clocksource

	echo "none" > answers
	KEYMAPOPTS=none \
		HOSTNAMEOPTS=alpine \
		INTERFACESOPTS=done \
		DNSOPTS=none \
		TIMEZONEOPTS=none \
		PROXYOPTS=none \
		APKREPOSOPTS=none \
		USEROPTS=none \
		SSHDOPTS=none \
		DISKOPTS=none \
		LBUOPTS=none \
		APKCACHEOPTS=none \
		\
		atf_check -s exit:0 \
		-o not-match:"Which NTP client to run" \
		setup-alpine -e < answers
}

setup_alpine_restart_network_body() {
	init_env
	mkdir -p sys/class/net/lo sys/class/net/eth0
	echo 1 >sys/class/net/lo/ifindex
	echo 2 >sys/class/net/eth0/ifindex
	echo down >sys/class/net/lo/operstate
	echo down >sys/class/net/eth0/operstate

	SSH_CONNECTION="::1 54984 ::1 22" atf_check -s exit:0 \
		-o match:"openrc boot" \
		-e empty \
		setup-alpine -q
	atf_check -s exit:3 \
		rc-service --quiet hostname status
	atf_check -s exit:3 \
		rc-service --quiet networking status
}
